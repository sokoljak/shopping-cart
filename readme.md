# Shopping cart project

### Calculates purchasable cart items based on stock and price restriction.
Main goal is to provide mechanism to define new restrictions without code modification.
To achieve this the chain of responsibility pattern is used.

Enjoy!