package pl.sokol.purchasable.item.service

import pl.sokol.purchasable.item.model.CartItem
import pl.sokol.purchasable.item.model.Stock
import pl.sokol.purchasable.item.processor.CartItemPriceRestrictionProcessor
import pl.sokol.purchasable.item.processor.CartItemRestrictionProcessorBuilder
import pl.sokol.purchasable.item.processor.CartItemStockRestrictionProcessor
import spock.lang.Specification

class PurchasableCartItemImplSpec extends Specification {

    def stockList = [new Stock(1, "t-shirt-s", 4),
                     new Stock(2, "t-shirt-m", 1),
                     new Stock(3, "shoes-43", 2)]

    def cartItemRestrictionProcessor = new CartItemRestrictionProcessorBuilder().with(new CartItemStockRestrictionProcessor(stockList))
                                                                                .with(new CartItemPriceRestrictionProcessor(new BigDecimal(200)))
                                                                                .build()
    def sut = new PurchasableCartItemImpl(cartItemRestrictionProcessor)

    def "should return two t-shirts-s as purchasable items"() {
        given:
            def cartItemList = [new CartItem(1, "t-shirt-s", new BigDecimal(40)),
                                new CartItem(1, "t-shirt-s", new BigDecimal(80)),
                                new CartItem(2, "t-shirt-m", new BigDecimal(120)),
                                new CartItem(2, "t-shirt-m", new BigDecimal(180)),
                                new CartItem(3, "skirt-m", new BigDecimal(200))]

        when:
            def purchasableItems = sut.calculatePurchasableItems(cartItemList)

        then:
            purchasableItems == [new CartItem(1, "t-shirt-s", new BigDecimal(40)),
                                 new CartItem(1, "t-shirt-s", new BigDecimal(80))]
    }
}