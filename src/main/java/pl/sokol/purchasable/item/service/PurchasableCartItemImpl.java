package pl.sokol.purchasable.item.service;

import lombok.RequiredArgsConstructor;
import pl.sokol.purchasable.item.model.CartItem;
import pl.sokol.purchasable.item.processor.CartItemRestrictionProcessor;

import java.util.List;

@RequiredArgsConstructor
public class PurchasableCartItemImpl implements PurchasableCartItem {

    private final CartItemRestrictionProcessor cartItemRestrictionProcessor;

    @Override
    public List<CartItem> calculatePurchasableItems(List<CartItem> cartItemList) {
        return cartItemRestrictionProcessor.calculatePurchasableItems(cartItemList.stream());
    }
}