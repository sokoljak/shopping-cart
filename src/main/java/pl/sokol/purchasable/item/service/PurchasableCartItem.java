package pl.sokol.purchasable.item.service;

import pl.sokol.purchasable.item.model.CartItem;

import java.util.List;

public interface PurchasableCartItem {

    List<CartItem> calculatePurchasableItems(List<CartItem> cartItemList);
}