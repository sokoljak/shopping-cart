package pl.sokol.purchasable.item.model;

import lombok.Value;

import java.math.BigDecimal;

@Value
public class CartItem {

    int id;
    String productId;
    BigDecimal price;
}