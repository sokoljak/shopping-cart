package pl.sokol.purchasable.item.model;

import lombok.Value;

@Value
public class Stock {

    int id;
    String productId;
    int quantity;

    public Stock newStockWithReducedQuantity() {
        return new Stock(id, productId, quantity - 1);
    }
}