package pl.sokol.purchasable.item.processor;

import pl.sokol.purchasable.item.model.CartItem;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public abstract class CartItemRestrictionProcessor {

    private CartItemRestrictionProcessor next;

    protected abstract boolean isAllowed(CartItem cartItem);

    public CartItemRestrictionProcessor nextProcessor(CartItemRestrictionProcessor next) {
        this.next = next;
        return next;
    }

    public List<CartItem> calculatePurchasableItems(Stream<CartItem> cartItemStream) {
        final Stream<CartItem> purchasableItemStream = cartItemStream.filter(this::isAllowed);
        return Optional.ofNullable(next).map(next -> next.calculatePurchasableItems(purchasableItemStream))
                                        .orElseGet(() -> purchasableItemStream.collect(Collectors.toList()));
    }
}