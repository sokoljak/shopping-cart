package pl.sokol.purchasable.item.processor;

import pl.sokol.purchasable.item.model.CartItem;
import pl.sokol.purchasable.item.model.Stock;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class CartItemStockRestrictionProcessor extends CartItemRestrictionProcessor {

    private final List<Stock> stockItems;

    public CartItemStockRestrictionProcessor(List<Stock> stockItems) {
        this.stockItems = new ArrayList<>(stockItems);
    }

    @Override
    protected boolean isAllowed(CartItem cartItem) {
        final Optional<Stock> first = stockItems.stream().filter(s -> cartItem.getProductId().equals(s.getProductId()))
                                                         .findFirst();
        if (first.isPresent()) {
            final Stock stock = first.get();
            if (stock.getQuantity() > 0) {
                stockItems.remove(stock);
                stockItems.add(stock.newStockWithReducedQuantity());
                return true;
            }
        }
        return false;
    }
}