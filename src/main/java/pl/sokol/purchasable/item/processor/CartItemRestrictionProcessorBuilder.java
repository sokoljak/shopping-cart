package pl.sokol.purchasable.item.processor;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class CartItemRestrictionProcessorBuilder {

    private CartItemRestrictionProcessor root;
    private CartItemRestrictionProcessor current;

    public CartItemRestrictionProcessorBuilder with(CartItemRestrictionProcessor processor) {
        if (root == null) {
            root = processor;
            current = processor;
        } else {
            current = current.nextProcessor(processor);
        }
        return this;
    }

    public CartItemRestrictionProcessor build() {
        return root;
    }
}