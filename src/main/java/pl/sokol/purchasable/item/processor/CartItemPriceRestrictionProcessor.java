package pl.sokol.purchasable.item.processor;

import pl.sokol.purchasable.item.model.CartItem;

import java.math.BigDecimal;

public class CartItemPriceRestrictionProcessor extends CartItemRestrictionProcessor {

    private final BigDecimal maxPrice;
    private BigDecimal currentPrice;

    public CartItemPriceRestrictionProcessor(BigDecimal maxPrice) {
        this.maxPrice = maxPrice;
        this.currentPrice = new BigDecimal(0);
    }

    @Override
    protected boolean isAllowed(CartItem cartItem) {
        if (currentPrice.add(cartItem.getPrice()).compareTo(maxPrice) <= 0) {
            currentPrice = currentPrice.add(cartItem.getPrice());
            return true;
        }
        return false;
    }
}