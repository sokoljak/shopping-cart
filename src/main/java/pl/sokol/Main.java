package pl.sokol;

import pl.sokol.purchasable.item.model.CartItem;
import pl.sokol.purchasable.item.model.Stock;
import pl.sokol.purchasable.item.processor.CartItemPriceRestrictionProcessor;
import pl.sokol.purchasable.item.processor.CartItemRestrictionProcessor;
import pl.sokol.purchasable.item.processor.CartItemRestrictionProcessorBuilder;
import pl.sokol.purchasable.item.processor.CartItemStockRestrictionProcessor;
import pl.sokol.purchasable.item.service.PurchasableCartItem;
import pl.sokol.purchasable.item.service.PurchasableCartItemImpl;

import java.math.BigDecimal;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        final Stock tshirtS = new Stock(1, "t-shirt-s", 4);
        final Stock tshirtM = new Stock(2, "t-shirt-m", 1);
        final Stock shoes43 = new Stock(2, "shoes-43", 2);

        final CartItem cartItemTshirtS1 = new CartItem(1, "t-shirt-s", new BigDecimal(40));
        final CartItem cartItemTshirtS2 = new CartItem(1, "t-shirt-s", new BigDecimal(80));

        final CartItem cartItemTshirtM1 = new CartItem(2, "t-shirt-m", new BigDecimal(120));
        final CartItem cartItemTshirtM2 = new CartItem(2, "t-shirt-m", new BigDecimal(180));

        final CartItem cartItemSkirtM1 = new CartItem(3, "skirt-m", new BigDecimal(200));

        final List<CartItem> cartItemList = List.of(cartItemTshirtS1, cartItemTshirtS2, cartItemTshirtM1, cartItemTshirtM2, cartItemSkirtM1);
        final List<Stock> stockList = List.of(tshirtS, tshirtM, shoes43);

        final CartItemRestrictionProcessor restrictionProcessor = new CartItemRestrictionProcessorBuilder().with(new CartItemStockRestrictionProcessor(stockList))
                                                                                                           .with(new CartItemPriceRestrictionProcessor(new BigDecimal(200)))
                                                                                                           .build();

        final PurchasableCartItem purchasableCartItem = new PurchasableCartItemImpl(restrictionProcessor);
        List<CartItem> purchasableItems = purchasableCartItem.calculatePurchasableItems(cartItemList);

        System.out.println(purchasableItems);
    }
}